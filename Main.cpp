#include <iostream>
class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animal voice";
	}
};

class Dog: public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};
class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};
class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Muu!\n";
	}
};
class Sheep : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Baa!\n";
	}
};


int main()
{
	Animal* a[4];
	a[0] = new Sheep;
	a[1] = new Dog;
	a[2] = new Cow;
	a[3] = new Cat;
	
	for (auto ai : a)
	{
		ai->Voice();

	}
	
}